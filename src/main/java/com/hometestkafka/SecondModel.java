package com.hometestkafka;

public class SecondModel {
	
	private String title;
	private String description;
	
	
	public SecondModel(String title, String description) {
		super();
		this.title = title;
		this.description = description;
	}
	
	public SecondModel() {}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "SecondModel [title=" + title + ", description=" + description + "]";
	}
	

	

}
