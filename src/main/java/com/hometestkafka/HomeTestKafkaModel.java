package com.hometestkafka;

public class HomeTestKafkaModel {
	
	private String FName;
	private String LName;
	
	public HomeTestKafkaModel(){
		
	}
	
	public HomeTestKafkaModel(String fname, String lname) {
		
		this.FName = fname;
		this.LName = lname;
	}

	public String getFName() {
		return FName;
	}

	public void setFName(String fName) {
		FName = fName;
	}

	public String getLName() {
		return LName;
	}

	public void setLName(String lName) {
		LName = lName;
	}
	
	

}
