package com.hometestkafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

@RestController
@RequestMapping("api/kafka")
public class HomeTestKafkaController {
	
	private KafkaTemplate<String, String> kafkaTemplate;
	private Gson jsonConverter;
	
	@Autowired
	public HomeTestKafkaController(KafkaTemplate<String, String> kafkaTemplate, Gson jsonConverter) {
		
		this.kafkaTemplate = kafkaTemplate;
		this.jsonConverter = jsonConverter;
		
	}
	
	@PostMapping
	public void post(@RequestBody HomeTestKafkaModel hometestkafkaModel) {
		
	    String topic;
		kafkaTemplate.send(topic = "myTopic", jsonConverter.toJson(hometestkafkaModel));

	}
	
	@PostMapping("/v2")
	public void post(@RequestBody SecondModel secondModel) {
		String topic;
		kafkaTemplate.send(topic = "myTopic2", jsonConverter.toJson(secondModel));
				
	}
	
	@KafkaListener(topics = "myTopic")
	public void getFromKafka(String hometestkafkaModel) {
		System.out.println(hometestkafkaModel);
		
		HomeTestKafkaModel hometestkafkaModel1  = (HomeTestKafkaModel) jsonConverter.fromJson(hometestkafkaModel, HomeTestKafkaModel.class);
		
		System.out.println(hometestkafkaModel1.toString());
	}
	
	@KafkaListener(topics = "myTopic2")
	public void getFromKafka2(String secondModel) {
		System.out.println(secondModel);
		
		SecondModel secondModel1  = (SecondModel) jsonConverter.fromJson(secondModel, SecondModel.class);
		
		System.out.println(secondModel1.toString());
	}
	
	
}
