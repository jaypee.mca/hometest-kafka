package com.hometestkafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HomeTestKafkaApplication {

	public static void main(String[] args) {
		SpringApplication.run(HomeTestKafkaApplication.class, args);
	}

}
